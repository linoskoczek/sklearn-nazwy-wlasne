# Rozpoznawanie nazw własnych na podstawie pliku
## s15237

### Treść zadania

Wykorzystując pakiet sklearn-crfsuite (lub inny pakiet/program  implementujący metodę CRF, np. crf++) wytrenuj model rozpoznający nazwy własne na podstawie pliku  nkjp-morph-named.zip (na ftp) . Zbiór należy podzielić w proporcjach 80%/10%/10% na zbiór treningowy, walidacyjny i testowy. Zbiór walidacyjny służyć ma do doboru  cech, a zbiór testowy do ostatecznej ewaluacji. Można wykorzystać dowolną metodę uczenia statystycznego. Należy przetestować parę  wariantów na danych walidacyjnych i co najmniej dwa najlepsze na danych testowy i podać wyniki miary F1 na etykietach z wyłączeniem etykiety ‘O’.

Gdyby uczenie modelu zabierało zbyt wiele czasu, można ograniczyć  rozmiar danych (wziąć część pliku)

Wyniki należy umieścić na platformie edux lub przysłać na mailem (ew. podać link) na adres: agn@pjwstk.edupl.; w temacie wpisać: INL-zadanie-2. Wyniki  powinny zawierać kod programu, opis przeprowadzonych testów i uzyskanych wyników oraz ewentualne wnioski z ich analizy

___

### Opis rozwiązania

Przedstawione rozwiązanie bazuje na 250 000 linijek z pliku *nkjp-morph-named.txt*. Czas działania programu na moim komputerze to kilkanaście minut. 

W pierwszej fazie dane są oczyszczane z tych, których etykietą jest "O". Następnie sieć uczy się na 80% pozostałych danych. Po skończonej nauce, 10% danych jest użyte do odnalezienia najlepszych parametrów C1 i C2 do CRF. Wybierane są 2 najlepsze pary i to na nich są wykonywane testy na ostatnich 10% danych.

### Wyniki

Na koniec dzialania programu wyświetlone zostaną dwa duże raporty zawierające wyniki przeprowadzenia testów.
Średnia ważona wartości F1 to 0.75.

```
   micro avg       0.79      0.72      0.75      1855
   macro avg       0.62      0.52      0.56      1855
weighted avg       0.78      0.72      0.75      1855
```

### Wnioski

W czasie pisania programu zauważyłem, że manipulacja parametrami C1 i C2 daje efekty i istotnie wpływa na późniejsze działanie sieci. Limitowanie liczby iteracji również potrafi zmienić rezultaty. 
W programie testy przeprowadzone na dwóch parach współczynników C1 i C2 dają te same wyniki. Myślę, że jest to spowodowane dużą ilością danych, na których sieć została nauczona.

### Źródła wiedzy

Zadanie wykonane było na podstawie wielu poradników dostępnych w sieci. Oto te, które najbardziej się przydały:
* https://ksopyla.com/data-science/precision-recall-f1-miary-oceny-klasyfikatora/
* https://eli5.readthedocs.io/en/latest/tutorials/sklearn_crfsuite.html
* https://github.com/steveneale/ner_crf/blob/master/ner_crf.ipynb
* https://towardsdatascience.com/named-entity-recognition-with-nltk-and-spacy-8c4a7d88e7da
* https://www.aitimejournal.com/@akshay.chavan/complete-tutorial-on-named-entity-recognition-ner-using-python-and-keras


