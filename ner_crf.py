import math
import warnings
import pyprind
import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.model_selection import learning_curve
from sklearn.model_selection import RandomizedSearchCV
from sklearn_crfsuite import CRF
from sklearn_crfsuite import metrics
from sklearn.metrics import make_scorer, precision_recall_fscore_support
from sklearn.exceptions import UndefinedMetricWarning

# Read the NER data using spaces as separators, keeping blank lines and adding columns
input_file = "./nkjp-morph-named.txt"
size = 250000
ner_data = pd.read_csv(input_file, sep="\t", header=None, skip_blank_lines=False, encoding="utf-8", nrows=size)
ner_data.columns = ["token", "orph", "chunk", "ne"]

# Explore the distribution of NE tags in the dataset
tag_distribution = ner_data.groupby("ne").size().reset_index(name='counts')
print(tag_distribution)

# Extract the useful classes (not 'O' or NaN values) as a list
classes = list(filter(lambda x: x not in ["O", np.nan], list(ner_data["ne"].unique())))

print(classes)

# Create a sentences dictionary and an initial single sentence dictionary
sentences, sentence = [], []
# Create a progress bar
pbar = pyprind.ProgBar(len(ner_data))
# For each row in the NER data...
for index, row in ner_data.iterrows():
    # If the row is empty (no string in the token column)
    if row["token"] == '.':
        # If the current sentence is not empty, append it to the sentences and create a new sentence
        if len(sentence) > 0:
            sentences.append(sentence)
            sentence = []
    # Otherwise...
    else:
        # If the row does not indicate the start of a document, add the token to the current sentence
        if type(row["token"]) != float and type(row["orph"]) != float and type(row["ne"]) != float:
            if not row["token"].startswith("-DOCSTART-"):
                sentence.append([row["token"], row["orph"], row["chunk"], row["ne"]])
    pbar.update()

def word_features(sentence, i, use_chunks=False):
    # Get the current word and orph
    word = sentence[i][0]
    orph = sentence[i][1]
    # Create a feature dictionary, based on characteristics of the current word and orph
    features = { "bias": 1.0,
                 "word.lower()": word.lower(),
                 "word[-3:]": word[-3:],
                 "word[-2:]": word[-2:],
                 "word.isupper()": word.isupper(),
                 "word.istitle()": word.istitle(),
                 "word.isdigit()": word.isdigit(),
                 "orph": orph,
                 "orph[:2]": orph[:2],
               }
    # If chunks are being used, add the current chunk to the feature dictionary
    if use_chunks:
        chunk = sentence[i][2]
        features.update({ "chunk": chunk })
    # If this is not the first word in the sentence...
    if i > 0:
        # Get the sentence's previous word and orph
        prev_word = sentence[i-1][0]
        prev_orph = sentence[i-1][1]
        # Add characteristics of the sentence's previous word and orph to the feature dictionary
        features.update({ "-1:word.lower()": prev_word.lower(),
                          "-1:word.istitle()": prev_word.istitle(),
                          "-1:word.isupper()": prev_word.isupper(),
                          "-1:orph": prev_orph,
                          "-1:orph[:2]": prev_orph[:2],
                        })
        # If chunks are being used, add the previous chunk to the feature dictionary
        if use_chunks:
            prev_chunk = sentence[i-1][2]
            features.update({ "-1:chunk": prev_chunk })
    # Otherwise, add 'BOS' (beginning of sentence) to the feature dictionary
    else:
        features["BOS"] = True
    # If this is not the last word in the sentence...
    if i < len(sentence)-1:
        # Get the sentence's next word and orph
        next_word = sentence[i+1][0]
        next_orph = sentence[i+1][1]
        # Add characteristics of the sentence's previous next and orph to the feature dictionary
        features.update({ "+1:word.lower()": next_word.lower(),
                          "+1:word.istitle()": next_word.istitle(),
                          "+1:word.isupper()": next_word.isupper(),
                          "+1:orph": next_orph,
                          "+1:orph[:2]": next_orph[:2],
                        })
        # If chunks are being used, add the next chunk to the feature dictionary
        if use_chunks:
            next_chunk = sentence[i+1][2]
            features.update({ "+1:chunk": next_chunk })
    # Otherwise, add 'EOS' (end of sentence) to the feature dictionary
    else:
        features["EOS"] = True
    # Return the feature dictionary
    return features

# Return a feature dictionary for each word in a given sentence
def sentence_features(sentence, use_chunks=False):
    return [word_features(sentence, i, use_chunks) for i in range(len(sentence))]

# Return the label (NER tag) for each word in a given sentence
def sentence_labels(sentence):
    return [label for token, orph, chunk, label in sentence]

def report(results, n_top=2):
    c_list = []
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            print("Model with rank: {0}".format(i))
            print("Mean validation score: {0:.3f} (std: {1:.3f})"
                  .format(results['mean_test_score'][candidate],
                          results['std_test_score'][candidate]))
            print("Parameters: {0}".format(results['params'][candidate]))
            print("")
            c_list.append((results['params'][candidate]['c1'], results['params'][candidate]['c2']))
    return c_list

# For each sentence, extract the sentence features as X, and the labels as y
X = [sentence_features(sentence) for sentence in sentences]
y = [sentence_labels(sentence) for sentence in sentences]

# Split X and y into training (80%), validation (10%) and test (10%)
# First divide 80% training from 20% test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)
# Then divide 20% test in halves
X_test, X_val, y_test, y_val = train_test_split(X_test, y_test, test_size=0.5, random_state=1)

# Create a new CRF model
crf = CRF(algorithm="lbfgs",
          max_iterations=200,
          all_possible_transitions=True)


# Set up a parameter grid to experiment with different values for C1 and C2
param_range = [0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0, 1000.0]
param_grid = {"c1": param_range,
              "c2": param_range}

# Set up a bespoke scorer that will compare the cross validated models according to their F1 scores
f1_scorer = make_scorer(metrics.flat_f1_score, average='weighted', labels=classes)

# Perform a 3-fold cross-validated, randomised search of 50 combinations for different values for C1 and C2
rs = RandomizedSearchCV(estimator=crf,
                        param_distributions=param_grid,
                        scoring=f1_scorer,
                        cv=3,
                        verbose=1,
                        n_iter=100,
                        n_jobs=-1)

# Train the models in the randomised search, ignoring any 'UndefinedMetricWarning' that comes up 
with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=UndefinedMetricWarning)
    warnings.filterwarnings("ignore", category=FutureWarning)
    rs.fit(X_val, y_val)

    c_list = report(rs.cv_results_, 2)

    for c in c_list:
        # Try best known C1 and C2 values for an estimator
        crf = CRF(algorithm="lbfgs",
            max_iterations=200,
            c1 = c[0],
            c2 = c[1])
        print(crf)
        
        crf.fit(X_train, y_train)

        # Use the CRF model to make predictions on the test data
        y_pred = crf.predict(X_test)
        print("== Results for ", c, "==")
        print(metrics.flat_classification_report(y_test, y_pred, labels=classes))